package br.com.labmcs.designpatterns.state;


public interface EstadoDoOrcamento {
	
	public void aprova(Orcamento orcamento);
	
	public void reprova(Orcamento orcamento);

}
