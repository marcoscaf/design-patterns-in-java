package br.com.labmcs.designpatterns.state;


public class Orcamento {
	
	double valor;
	
	// Interface que da um estado para o orçamento.
	// Cada novo orçamento criado, já inicia no estado Em Aprovação
	EstadoDoOrcamento estado;
	
	public Orcamento() {
		estado = new EstadoEmAprovacao();
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}
	
	public void reprova() {
		estado.reprova(this);
	}
	
	public void aprova() {
		estado.aprova(this);
	}

}
