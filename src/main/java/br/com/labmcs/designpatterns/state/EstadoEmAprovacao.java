package br.com.labmcs.designpatterns.state;


public class EstadoEmAprovacao implements EstadoDoOrcamento{

	public void aprova(Orcamento orcamento) {
		// realiza algum outro processamento necessário antes de aprovar...
		//
		// Aprova o orçamento
		orcamento.estado = new EstadoAprovado();
	}

	public void reprova(Orcamento orcamento) {
		orcamento.estado = new EstadoReprovado();
		
	}


}
