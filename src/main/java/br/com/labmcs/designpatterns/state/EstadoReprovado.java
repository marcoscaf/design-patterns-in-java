package br.com.labmcs.designpatterns.state;


public class EstadoReprovado implements EstadoDoOrcamento {

	public void aprova(Orcamento orcamento) {
		throw new UnsupportedOperationException("Orçamento já reprovado");
	}

	public void reprova(Orcamento orcamento) {
		throw new UnsupportedOperationException("Orçamento já reprovado");
	}
	
	public String toString() {
		return "Orçamento Reprovado";
	}
}
