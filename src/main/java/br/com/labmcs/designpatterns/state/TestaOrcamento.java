package br.com.labmcs.designpatterns.state;


public class TestaOrcamento {

	public static void main(String[] args) {
		Orcamento orcamento = new Orcamento();
		// 
		// Ececu�o de algum c�digo
		//
		// Aprovando o orçamento...
		orcamento.aprova();
		
		// tenta reprovar um orçamento já aprovado...
		// orcamento.reprova(); // deve lançar um erro
		
		
		// Tenta aprovar novamente o orçamento.
		// orcamento.aprova(); // deve lançar um erro
		
		System.out.println(orcamento.estado);
	}
}
