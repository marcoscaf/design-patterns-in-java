package br.com.labmcs.designpatterns.state;

public class EstadoAprovado implements EstadoDoOrcamento {

	public void aprova(Orcamento orcamento) {
		throw new UnsupportedOperationException("Orçamento já aprovado");
	}

	public void reprova(Orcamento orcamento) {
		throw new UnsupportedOperationException("Orçamento já aprovado");
	}

	public String toString() {
		return "Orçamento Aprovado";
	}
}
