package br.com.labmcs.designpatterns.proxy;


public class MainClass {

	public static void main(String[] args) {
		
		// Criando um Proxy de SystemProperties
		SystemProperties sp = SystemPropertieRetriever.create(SystemProperties.class);
		
		System.out.println(sp.getJavaHome());
		System.out.println(sp.getJavaVmSpecificationVersion());
	}

}
