package br.com.labmcs.designpatterns.proxy;

public class SystemPropertiesReader implements SystemProperties {

	public String getUserCountry() {
		return System.getProperty("user.country");
	}

	public String getUserLanguage() {
		return System.getProperty("user.country");
	}

	public String getUserHome() {
		return System.getProperty("user.home");
	}

	public String getJavaVmSpecificationVersion() {
		return System.getProperty("java.vm.specification.version");
	}

	public String getJavaHome() {
		return System.getProperty("java.home");
	}

}
