package br.com.labmcs.designpatterns.proxy;



import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class SystemPropertiesProxy implements InvocationHandler {
	
	private SystemPropertiesReader systemPropertiesReader;

	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		
		systemPropertiesReader = new SystemPropertiesReader();
		
		// Este m�todo "invoke" tem um comportamento de interceptador de m�todo
		// CHamando a implementa��o do objeto real que cont�m a implementa��o do m�todo
		
		return method.invoke(systemPropertiesReader, args);
	}
	
	
	// M�todos n�o utilizados.....
	private String[] splitByUpperCase(String code) {
		List<String> list = new ArrayList<String>();
		String current = "";
		for (int i = 0; i < code.length(); i++) {
			if (Character.isUpperCase(code.charAt(i))) {
				list.add(current);
				current = "";
				current += Character.toLowerCase(code.charAt(i));
			} else {
				current += code.charAt(i);
			}
		}
		list.add(current);
		return list.toArray(new String[list.size()]);
	}

	private String getPropertieName(String[] args) {
		String propName = "";
		for (int i = 1; i < args.length; i++) {
			if (i != 1) {
				propName += ".";
			}
			propName += args[i];

		}
		return propName;
	}


}
