package br.com.labmcs.designpatterns.proxy;



import java.lang.reflect.Proxy;

public class SystemPropertieRetriever  {
	
	@SuppressWarnings("unchecked")
	public static <E> E create(Class<E> clazz) {
		
		// Proxy.newProxyInstance, cria um proxy 
		// Parametros:
		// classloader do objeto passado
		// interface que a classe implementa (ou passar ela mesma)
		// instância que terá o método invoke chamado.
		return (E) Proxy.newProxyInstance(
				clazz.getClassLoader(), 
				new Class[] { clazz },
				new SystemPropertiesProxy());
	}
	 

	
}
