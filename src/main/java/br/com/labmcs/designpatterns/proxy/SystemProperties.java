package br.com.labmcs.designpatterns.proxy;



public interface SystemProperties {

	public String getUserCountry();

	public String getUserLanguage();

	public String getUserHome();

	public String getJavaVmSpecificationVersion();

	public String getJavaHome();
}
