package br.com.labmcs.designpatterns.visitor;

public interface Expression {
	
	public int interpret();
	public void accept(Visitor visitor);

}
