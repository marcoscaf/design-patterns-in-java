package br.com.labmcs.designpatterns.visitor;

public interface Visitor {

	public void visitSum(Sum sum);

	public void visitSubtraction(Subtraction subtraction);

	public void visitMultiplication(Multiplication multiplication);

	public void visitNumber(Number number);

}
