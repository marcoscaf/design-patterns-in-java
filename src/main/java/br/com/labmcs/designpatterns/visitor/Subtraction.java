package br.com.labmcs.designpatterns.visitor;

public class Subtraction extends AbstractExpression {

	private Expression leftExpression;
	private Expression rightExpression;

	public Subtraction(Expression leftExpression, Expression rightExpression) {
		super(leftExpression, rightExpression);
		this.leftExpression = leftExpression;
		this.rightExpression = rightExpression;
	}

	public int interpret() {
		return getLeftExpression().interpret() - getRightExpression().interpret();
	}

	public void accept(Visitor visitor) {
		visitor.visitSubtraction(this);
	}

	public Expression getLeftExpression() {
		return leftExpression;
	}

	public Expression getRightExpression() {
		return rightExpression;
	}

}
