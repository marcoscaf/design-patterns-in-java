package br.com.labmcs.designpatterns.visitor;

public class Calculator {

	public static void main(String[] args) {
		
		Visitor visitor = new PrinterVisitor();
		
		// Express�o: 5*(2+2)
		Expression expressionA = new Multiplication(new Number(5), new Sum(new Number(2), new Number(2)));
		expressionA.accept(visitor);
		
		System.out.println("\nValor expres�o: "+ (expressionA.interpret()));
		
		// Express�o A = (5*(10-(2+2)))+(5)
		Expression expressionB = 
				new Sum(
						new Multiplication(
								new Number(5),
								new Subtraction(
										new Number(10), 
										new Sum(
												new Number(2), 
												new Number(2)))), 
						new Number(5));
		System.out.println("\n");
		expressionB.accept(visitor);
		
		
		System.out.println("\nValor expres�o: "+ (expressionB.interpret()));

	}
}