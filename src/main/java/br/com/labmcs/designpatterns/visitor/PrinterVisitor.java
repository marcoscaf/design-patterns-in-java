package br.com.labmcs.designpatterns.visitor;

public class PrinterVisitor implements Visitor {

	public void visitSum(Sum sum) {

		System.out.print("(");
		sum.getLeftExpression().accept(this);
		System.out.print("+");
		sum.getRightExpression().accept(this);
		System.out.print(")");

	}

	public void visitSubtraction(Subtraction subtraction) {

		System.out.print("(");
		subtraction.getLeftExpression().accept(this);
		System.out.print("-");
		subtraction.getRightExpression().accept(this);
		System.out.print(")");

	}

	public void visitMultiplication(Multiplication multiplication) {

		System.out.print("(");
		multiplication.getLeftExpression().accept(this);
		System.out.print("*");
		multiplication.getRightExpression().accept(this);
		System.out.print(")");

	}

	public void visitNumber(Number number) {
		System.out.print(String.valueOf(number.getNumber()));
	}

}
