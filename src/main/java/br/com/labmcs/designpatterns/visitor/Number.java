package br.com.labmcs.designpatterns.visitor;

public class Number implements Expression {

	int number;

	public Number(int number) {
		this.number = number;
	}

	public int interpret() {
		return number;
	}

	public int getNumber() {
		return number;
	}

	public void accept(Visitor visitor) {
		visitor.visitNumber(this);
	}

}
