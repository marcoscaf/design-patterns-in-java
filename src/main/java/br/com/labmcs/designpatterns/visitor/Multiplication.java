package br.com.labmcs.designpatterns.visitor;

public class Multiplication extends AbstractExpression {

	private Expression leftExpression;
	private Expression rightExpression;

	public Multiplication(Expression leftExpression, Expression rightExpression) {
		super(leftExpression, rightExpression);
		this.leftExpression = leftExpression;
		this.rightExpression = rightExpression;
	}

	public int interpret() {
		return getLeftExpression().interpret() * getRightExpression().interpret();
	}

	public void accept(Visitor visitor) {
		visitor.visitMultiplication(this);
	}

	public Expression getLeftExpression() {
		return leftExpression;
	}

	public Expression getRightExpression() {
		return rightExpression;
	}

}
