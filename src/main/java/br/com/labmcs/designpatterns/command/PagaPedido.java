package br.com.labmcs.designpatterns.command;

public class PagaPedido implements Command {

	private Pedido pedido;

	public PagaPedido(Pedido pedido) {

		this.pedido = pedido;
	}

	public void process() {
		pedido.paga();
	}

}
