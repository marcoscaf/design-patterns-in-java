package br.com.labmcs.designpatterns.command;

public interface Command {
	
	void process();

}
