package br.com.labmcs.designpatterns.command;

public class Programa {
	
	public static void main(String[] args) {
		
		// Criando v�rios pedidos
		Pedido p1 = new Pedido("Marcos",50.0);
		Pedido p2 = new Pedido("Ana",800.0);
		Pedido p3 = new Pedido("Fl�via",150.0);
		Pedido p4 = new Pedido("Cl�udia",69.0);
		
		// Configurando uma fila de execu��o com Comandos espec�ficos
		FilaDeTrabalho filaDeTrabalho = new FilaDeTrabalho();
		filaDeTrabalho.addCommand(new PagaPedido(p1));
		filaDeTrabalho.addCommand(new ConcluiPedido(p2));
		filaDeTrabalho.addCommand(new PagaPedido(p3));
		filaDeTrabalho.addCommand(new ConcluiPedido(p4));
		
		// Executando a fila.
		filaDeTrabalho.processCommands();
	}
}
