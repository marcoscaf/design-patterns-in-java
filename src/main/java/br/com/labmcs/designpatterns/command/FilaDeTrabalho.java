package br.com.labmcs.designpatterns.command;

import java.util.ArrayList;
import java.util.List;

public class FilaDeTrabalho {

	private List<Command> commandList;

	public FilaDeTrabalho() {
		commandList = new ArrayList<Command>();
	}

	public void addCommand(Command command) {
		this.commandList.add(command);
	}

	public void processCommands() {
		for (Command command : commandList) {
			command.process();
		}
	}
}
