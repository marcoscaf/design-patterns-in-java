package br.com.labmcs.designpatterns.command;

public class ConcluiPedido implements Command {

	private Pedido pedido;

	public ConcluiPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public void process() {
		pedido.finaliza();
	}

}
