package br.com.labmcs.designpatterns.command;

import java.util.Calendar;

public class Pedido {

	private String cliente;
	private double valor;
	private Status status;
	private Calendar dataFinalizacao;

	public Pedido(String cliente, double valor) {
		this.cliente = cliente;
		this.valor = valor;
	}

	public void paga() {
		
		status = Status.PAGO;
		System.out.println("Pedido do "+cliente+" no valor "+valor+" pago !");
	}

	public void finaliza() {
		dataFinalizacao = Calendar.getInstance();
		status = Status.ENTREGUE;
		System.out.println("Pedido do "+cliente+" finalizado !");
	}

	public enum Status {
		NOVO, PROCESSANDO, PAGO, ITEM_SEPARADO, ENTREGUE;
	}
}