package br.com.labmcs.designpatterns.builder;

import java.util.Calendar;

public class NotaFiscalBuilder {

	private String cnpj;
	private String descricao;
	private Double valor;
	private Calendar data;

	public NotaFiscalBuilder comCNPJ(String cnpj) {
		this.cnpj = cnpj;
		return this;
	}

	public NotaFiscalBuilder comDescricao(String descricao) {
		this.descricao = descricao;
		return this;
	}

	public NotaFiscalBuilder comValor(Double valor) {
		this.valor = valor;
		return this;
	}

	public NotaFiscalBuilder comData(Calendar data) {
		this.data = data;
		return this;
	}

	public NotaFiscal constroi() {
		return new NotaFiscal(cnpj, descricao, valor, data);
	}
}
