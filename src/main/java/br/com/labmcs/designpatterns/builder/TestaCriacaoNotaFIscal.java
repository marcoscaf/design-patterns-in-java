package br.com.labmcs.designpatterns.builder;

import java.util.Calendar;

public class TestaCriacaoNotaFIscal {

	public static void main(String[] args) {

		NotaFiscalBuilder builder = new NotaFiscalBuilder();
		
		
		NotaFiscal notafiscal = builder.comCNPJ("cnpj")
			   .comDescricao("Descri��o quakquer")
			   .comData(Calendar.getInstance())
			   .comValor(100D)
			   .constroi();
		
		System.out.println(notafiscal);

	}

}
