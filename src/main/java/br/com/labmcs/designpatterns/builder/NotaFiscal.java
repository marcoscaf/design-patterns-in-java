package br.com.labmcs.designpatterns.builder;

import java.util.Calendar;

public class NotaFiscal {

	private String cnpj;
	private String descricao;
	private Double Valor;
	private Calendar data;

	public NotaFiscal(String cnpj, String descricao, Double valor, Calendar data) {
		this.cnpj = cnpj;
		this.descricao = descricao;
		Valor = valor;
		this.data = data;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Double getValor() {
		return Valor;
	}

	public void setValor(Double valor) {
		Valor = valor;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}
	
	public String toString() {
		return "CNPJ:"+ cnpj +" - Descri��o: "+descricao+" - Valor: "+Valor+" - Data: "+data;
		
	}

}
