package br.com.labmcs.designpatterns.decorator;

public class IOF implements Imposto {
	
	Imposto impostoComposicao;
	
	public IOF(Imposto impostoComposicao) {
		this.impostoComposicao = impostoComposicao;
	}
	
	public IOF() {
		
	}

	public double calculaImposto(Orcamento orcamento) {
		
		double impostoComposto = 0;
		
		// calculando o desconto em "cadeia" para depois calcular o desconto da regra atual
		if (impostoComposicao != null) {
			impostoComposto = impostoComposicao.calculaImposto(orcamento);
		}
		
		// Calculando o desconto do ICMS + o desconto calculado em cadeia.
		double impostoICMS = orcamento.getValor() * 0.10; 
		
		return impostoComposto + impostoICMS;
	}

}
