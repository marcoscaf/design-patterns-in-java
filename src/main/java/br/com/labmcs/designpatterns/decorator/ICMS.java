package br.com.labmcs.designpatterns.decorator;

public class ICMS implements Imposto {
	
	Imposto impostoComposicao;
	
	public ICMS(Imposto impostoComposicao) {
		this.impostoComposicao = impostoComposicao;
	}
	
	public ICMS() {
		
	}

	public double calculaImposto(Orcamento orcamento) {
		
		double impostoEmCadeia = 0;
		
		// calculando o desconto em "cadeia" para depois calcular o desconto da regra atual
		if (impostoComposicao != null) {
			impostoEmCadeia = impostoComposicao.calculaImposto(orcamento);
		}
		
		// Calculando o desconto do ICMS + o desconto calculado em cadeia.
		double descontoICMS = orcamento.getValor() * 0.10; 
		
		return descontoICMS + impostoEmCadeia;
	}

}
