package br.com.labmcs.designpatterns.decorator;

public class ICS implements Imposto {

	Imposto impostoComposicao;
	
	public ICS(Imposto impostoComposicao) {
		this.impostoComposicao = impostoComposicao;
	}
	
	public ICS( ) {
	}


	public double calculaImposto(Orcamento orcamento) {

		double impostoComposto = 0;
		// calculando o Imposto em "cadeia" para depois calcular o Imposto da regra
		// atual
		if (impostoComposicao != null) {
			impostoComposto = impostoComposicao.calculaImposto(orcamento);
		}
		// Calculando o Imposto do ICMS + o Imposto calculado em cadeia.
		double impostoICMS = orcamento.getValor() * 0.10;

		return impostoICMS + impostoComposto;
	}

}
