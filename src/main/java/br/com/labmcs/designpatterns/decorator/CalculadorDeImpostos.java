package br.com.labmcs.designpatterns.decorator;


public class CalculadorDeImpostos {

	public static void main(String[] args) {
		
		Orcamento orcamento = new Orcamento();
		orcamento.setValor(1000);
		
		// Exemplo de Decorator, aqui temos um Imposto composto por outros dois
		Imposto impostoComposto = new ICMS(new ICS(new IOF()));
		
		System.out.println(impostoComposto.calculaImposto(orcamento));
		
	}
}
