package br.com.labmcs.designpatterns.decorator;

public interface Imposto {
	
	/**
	 * Calcula desconto.
	 *
	 * @param orcamento
	 * @return valor do desconto
	 */
	double calculaImposto(Orcamento orcamento);


}
