package br.com.labmcs.designpatterns.memento;

import java.util.Calendar;

import br.com.labmcs.designpatterns.memento.Contrato.TipoContrato;

public class Programa {

    public static void main(String[] args) {

        HistoricoMemento historico = new HistoricoMemento();

        Contrato contrato = new Contrato(Calendar.getInstance(), "Mauricio", TipoContrato.NOVO);
        historico.adiciona(contrato.salvaEstado());

        contrato.avanca();
        historico.adiciona(contrato.salvaEstado());

        contrato.avanca();
        historico.adiciona(contrato.salvaEstado());

        contrato.avanca();
        historico.adiciona(contrato.salvaEstado());
        
        System.out.println(historico.pega(3).getContrato().getTipo());
        
    }
}