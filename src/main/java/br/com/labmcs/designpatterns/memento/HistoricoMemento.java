package br.com.labmcs.designpatterns.memento;

import java.util.ArrayList;
import java.util.List;

public class HistoricoMemento {
	
	 private List<Estado> estadosSalvos = new ArrayList<Estado>();

	    public Estado pega(int index) {
	        return estadosSalvos.get(index);
	    }

	    public void adiciona(Estado estado) {
	        estadosSalvos.add(estado);
	    }

}
