package br.com.labmcs.designpatterns.chainOfResponsability;

public class IOF implements Imposto {

	Imposto proximoImposto;

	public double calculaImposto(Orcamento orcamento) {
		
		double descontoEmCadeia = 0;
		
		// calculando o desconto em "cadeia" para depois calcular o desconto da regra atual
		if (proximoImposto != null) {
			descontoEmCadeia = proximoImposto.calculaImposto(orcamento);
		}

		double impostoIOF = orcamento.getValor() * 0.05;

		return impostoIOF + descontoEmCadeia;
	}

	public void proximoImposto(Imposto imposto) {
		this.proximoImposto = imposto;

	}

}
