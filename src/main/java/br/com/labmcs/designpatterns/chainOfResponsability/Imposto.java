package br.com.labmcs.designpatterns.chainOfResponsability;

public interface Imposto {

	/**
	 * Calcula desconto.
	 *
	 * @param orcamento
	 * @return valor do desconto
	 */
	double calculaImposto(Orcamento orcamento);

	/**
	 * Todo Desconto tem um próximo desconto para chamar na cadeia.
	 *
	 * @param desconto
	 */
	void proximoImposto(Imposto desconto);

}
