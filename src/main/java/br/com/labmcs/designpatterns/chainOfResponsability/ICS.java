package br.com.labmcs.designpatterns.chainOfResponsability;
public class ICS implements Imposto {

	private Imposto proximoImposto;

	public double calculaImposto(Orcamento orcamento) {

		double ImpostoEmCadeia = 0;
		// calculando o Imposto em "cadeia" para depois calcular o Imposto da regra
		// atual
		if (proximoImposto != null) {
			ImpostoEmCadeia = proximoImposto.calculaImposto(orcamento);
		}
		// Calculando o Imposto do ICMS + o Imposto calculado em cadeia.
		double ImpostoICMS = orcamento.getValor() * 0.10;

		return ImpostoICMS + ImpostoEmCadeia;
	}

	public void proximoImposto(Imposto Imposto) {
		proximoImposto = Imposto;
	}

}
