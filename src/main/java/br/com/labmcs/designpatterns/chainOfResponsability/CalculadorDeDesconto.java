package br.com.labmcs.designpatterns.chainOfResponsability;


public class CalculadorDeDesconto {

	public static void main(String[] args) {

		Orcamento orcamento = new Orcamento();
		orcamento.setValor(1000);

		Imposto impostoICMS = new ICMS();
		Imposto impostoIOF = new IOF();
		Imposto impostoICS = new ICS();

		// Configurando quem é o próximo desconto a ser calculado na cadeia.
		impostoICMS.proximoImposto(impostoIOF);
		impostoIOF.proximoImposto(impostoICS);
		
		System.out.println(impostoICMS.calculaImposto(orcamento));

	}

}
