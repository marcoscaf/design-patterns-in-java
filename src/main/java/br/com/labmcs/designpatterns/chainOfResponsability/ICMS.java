package br.com.labmcs.designpatterns.chainOfResponsability;

public class ICMS implements Imposto {
	
	private Imposto proximoImposto;

	public double calculaImposto(Orcamento orcamento) {
		
		double impostoEmCadeia = 0;
		
		// calculando o desconto em "cadeia" para depois calcular o desconto da regra atual
		if (proximoImposto != null) {
			impostoEmCadeia = proximoImposto.calculaImposto(orcamento);
		}
		
		// Calculando o desconto do ICMS + o desconto calculado em cadeia.
		double descontoICMS = orcamento.getValor() * 0.10; 
		
		return descontoICMS + impostoEmCadeia;
	}

	public void proximoImposto(Imposto desconto) {
		proximoImposto = desconto;
	}

}
