package br.com.labmcs.designpatterns.chainOfResponsability;
public class Orcamento {
	
	double valor;

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

}
