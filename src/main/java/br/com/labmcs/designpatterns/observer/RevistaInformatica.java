package br.com.labmcs.designpatterns.observer;

import java.util.Observable;
import java.util.Observer;

public class RevistaInformatica extends Observable {
 
    private int edicao;
 
    public void setNovaEdicao(int novaEdicao) {
        this.edicao = novaEdicao;
         
        // indica que houve alter��o
        setChanged();
        
        // notifica os observadores
        notifyObservers("TEste Param");
    }
    

     
    public int getEdicao() {
        return this.edicao;
    }
     
    public static void main(String[] args) {
        //poderia receber a nova edicao atraves de um recurso externo
        int novaEdicao = 3;
        RevistaInformatica revistaInformatica = new RevistaInformatica();

        // Registra os observadores
        new Assinante2(revistaInformatica);
        new Assinante1(revistaInformatica);
        
        revistaInformatica.setNovaEdicao(4);
    }
 
}
 
class Assinante1 implements Observer {
     
    Observable revistaInformatica;
     
    int edicaoNovaRevista;
     
    public Assinante1(Observable revistaInformatica) {
        this.revistaInformatica = revistaInformatica;
        revistaInformatica.addObserver(this);
    }
     
    public void update(Observable revistaInfSubject, Object arg1) {
        if (revistaInfSubject instanceof RevistaInformatica) {
            RevistaInformatica revistaInformatica = (RevistaInformatica) revistaInfSubject;
            edicaoNovaRevista = revistaInformatica.getEdicao();
            System.out.println("ASSINANTE 1   -  Aten��o, j� chegou a mais uma edi��o da Revista Informatica. " +
                    "Esta � a sua edi��o n�mero: " + edicaoNovaRevista);
        }
    }   
}

class Assinante2 implements Observer {
    
    Observable revistaInformatica;
     
    int edicaoNovaRevista;
     
    public Assinante2(Observable revistaInformatica) {
        this.revistaInformatica = revistaInformatica;
        
        // Registra de fato osobservadores utilizando a pr�pria api java
        // Nessa linha estamos dizendo que a classe RevistaInformatica (que extende Observable)
        // ter� a classe atual (this) como observadora
        revistaInformatica.addObserver(this);
    }
    
    // m�todo sobrescrito que ser� invicado quando os observadores forem notificados.
    public void update(Observable revistaInfSubject, Object arg1) {
        if (revistaInfSubject instanceof RevistaInformatica) {
            RevistaInformatica revistaInformatica = (RevistaInformatica) revistaInfSubject;
            edicaoNovaRevista = revistaInformatica.getEdicao();
            System.out.println("ASSINANTE 2   -  Aten��o, j� chegou a mais uma edi��o da Revista Informatica. " +
                    "Esta � a sua edi��o n�mero: " + edicaoNovaRevista);
        }
    }   
}