package br.com.labmcs.designpatterns.strategy;

public class TesteCalculaImposto {
	
	public static void main(String[] args) {
		
		Orcamento orcamento = new Orcamento(500D);
		
		// Estratégias para cada imposto.
		Imposto icms = new ICMS();
		Imposto iss  = new ISS();
		
		CalculadoraDeImpostos calculadora = new CalculadoraDeImpostos();
		
		// Estratégia ICMS
		double impostoICMS = calculadora.calculaImposto(orcamento, icms);
		System.out.println(impostoICMS);

		// Estratégia ISS
		double impostoISS = calculadora.calculaImposto(orcamento, iss);
		System.out.println(impostoISS);
		
	
		
	}

}
