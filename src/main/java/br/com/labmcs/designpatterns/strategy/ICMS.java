package br.com.labmcs.designpatterns.strategy;

/**
 * Strategy ICMS
 */
public class ICMS  implements Imposto{

	public double calculaImporto(Orcamento orcamento) {
		return orcamento.getValorOrcamento() * 0.08;
	}
}
