package br.com.labmcs.designpatterns.strategy;


/**
 * Contrato do Strategy
 */
public interface Imposto {
	
	public double calculaImporto(Orcamento orcamento);

}
