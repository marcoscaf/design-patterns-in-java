package br.com.labmcs.designpatterns.strategy;


public class CalculadoraDeImpostos {

	/**
	 * Método que calcula um Importo qualquer, a variável 'imposto' é o que chamamos
	 * de Strategy, pois ela pode ser qualquer imposto
	 *
	 * @param orcamento
	 * @param imposto
	 * @return valor do imposto calculado
	 */
	public double calculaImposto(Orcamento orcamento, Imposto imposto) {

		return imposto.calculaImporto(orcamento);

	}

}
