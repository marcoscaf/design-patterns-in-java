package br.com.labmcs.designpatterns.strategy;


/**
 * Strategy ISS
 */
public class ISS  implements Imposto{

	public double calculaImporto(Orcamento orcamento) {
		return orcamento.getValorOrcamento() * 0.1;
	}

}
