package br.com.labmcs.designpatterns.strategy;

public class Orcamento {

	double valorOrcamento;

	public Orcamento(double valorOrcamento) {
		this.valorOrcamento = valorOrcamento;
	}

	public double getValorOrcamento() {
		return valorOrcamento;
	}

	public void setValorOrcamento(double valorOrcamento) {
		this.valorOrcamento = valorOrcamento;
	}
	
}
