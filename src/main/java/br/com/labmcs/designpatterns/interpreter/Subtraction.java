package br.com.labmcs.designpatterns.interpreter;

public class Subtraction extends AbstractExpression {

	public Subtraction(Expression leftExpression, Expression rightExpression) {
		super(leftExpression, rightExpression);
	}

	public int interpret() {
		return getLeftExpression().interpret() - getRightExpression().interpret();
	}
}
