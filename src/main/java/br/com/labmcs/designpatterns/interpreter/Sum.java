package br.com.labmcs.designpatterns.interpreter;

public class Sum extends AbstractExpression {

	public Sum(Expression leftExpression, Expression rightExpression) {
		super(leftExpression, rightExpression);
	}

	public int interpret() {
		return getLeftExpression().interpret() + getRightExpression().interpret();
	}

}
