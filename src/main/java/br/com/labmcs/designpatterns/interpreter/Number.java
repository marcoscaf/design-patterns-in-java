package br.com.labmcs.designpatterns.interpreter;

public class Number implements Expression {

	int number;

	public Number(int number) {
		this.number = number;
	}

	public int interpret() {
		return number;
	}

}
