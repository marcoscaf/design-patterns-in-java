package br.com.labmcs.designpatterns.interpreter;

public interface Expression {
	
	public int interpret();

}
