package br.com.labmcs.designpatterns.interpreter;

public abstract class AbstractExpression implements Expression {

	private Expression rightExpression;
	private Expression leftExpression;

	public AbstractExpression(Expression leftExpression, Expression rightExpression) {
		this.leftExpression = leftExpression;
		this.rightExpression = rightExpression;
	}

	public Expression getRightExpression() {
		return rightExpression;
	}

	public Expression getLeftExpression() {
		return leftExpression;
	}
}