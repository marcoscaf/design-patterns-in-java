package br.com.labmcs.designpatterns.interpreter;

public class Calculator {

	public static void main(String[] args) {
		
		
		// Express�o: 5*(2+2)
		Expression expressionA = new Multiplication(new Number(5), new Sum(new Number(2), new Number(2)));
		
		System.out.println("Prova Real express�o 1: "+ (5*(2+2)));
		System.out.println("Valor expres�o 1 OO: "+ (expressionA.interpret()));
		
		
		
		
		
		// Express�o A = (5*(10-(2+2)))+(5)
		Expression expressionB = 
				new Sum(
						new Multiplication(
								new Number(5),
								new Subtraction(
										new Number(10), 
										new Sum(
												new Number(2), 
												new Number(2)))), 
						new Number(5));

		System.out.println("\nProva Real express�o 2: "+ ((5*(10-(2+2)))+(5)));
		System.out.println("Valor expres�o 1 OO: "+ (expressionB.interpret()));

	}
}