package br.com.labmcs.designpatterns.interpreter;

public class Multiplication extends AbstractExpression {

	public Multiplication(Expression leftExpression, Expression rightExpression) {
		super(leftExpression, rightExpression);
	}

	public int interpret() {
		return getLeftExpression().interpret() * getRightExpression().interpret();
	}
}
