package br.com.labmcs.designpatterns.flyweight;

import java.util.HashMap;
import java.util.Map;

public class NotasMusicais {

	private static Map<String, Nota> notasMusicaisCache = new HashMap<String, Nota>();

	static {
		notasMusicaisCache.put("do", new Do());
		notasMusicaisCache.put("re", new Re());
		notasMusicaisCache.put("mi", new Mi());
		notasMusicaisCache.put("fa", new Fa());
		notasMusicaisCache.put("sol", new Sol());
		notasMusicaisCache.put("la", new La());
		notasMusicaisCache.put("si", new Si());

	}

	public static Nota pegaNota(String nota) {
		return notasMusicaisCache.get(nota);
	}

}
