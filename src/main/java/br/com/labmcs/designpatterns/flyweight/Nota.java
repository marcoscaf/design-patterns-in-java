package br.com.labmcs.designpatterns.flyweight;

public interface Nota {
	
	public String getSimbol();

}
