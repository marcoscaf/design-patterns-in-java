package br.com.labmcs.designpatterns.flyweight;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Programa {

	public static void main(String[] args) {
		
		
		NotasMusicais notas = new NotasMusicais();
		
		List<Nota> musica = new ArrayList<Nota>(Arrays.asList(
				NotasMusicais.pegaNota("do"),
				NotasMusicais.pegaNota("re"),
				NotasMusicais.pegaNota("mi"),
				NotasMusicais.pegaNota("fa"),
				NotasMusicais.pegaNota("fa"),
				NotasMusicais.pegaNota("fa")
				));
		
		System.out.println(musica);		
		
	}
}
