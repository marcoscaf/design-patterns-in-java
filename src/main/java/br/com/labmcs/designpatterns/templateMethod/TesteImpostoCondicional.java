package br.com.labmcs.designpatterns.templateMethod;

public class TesteImpostoCondicional {
	
	public static void main(String[] args) {
		
		TemplateImpostoCondicional imposto = new ImpostoCondocionalMinimaTax();
		
		Orcamento orcamento = new Orcamento(200);
		
		double valorImposto = imposto.calculaImporto(orcamento);
		
		System.out.println(valorImposto);
	}

}
