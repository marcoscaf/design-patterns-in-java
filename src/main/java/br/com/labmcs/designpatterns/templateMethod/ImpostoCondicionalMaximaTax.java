package br.com.labmcs.designpatterns.templateMethod;

public class ImpostoCondicionalMaximaTax extends TemplateImpostoCondicional {

	@Override
	protected boolean usaMaximaTaxacao(Orcamento orcamento) {
		// l�gica para usar maxima taxa��o
		if (orcamento.getValorOrcamento() > 100)
			return true;
		else
			return false;
	}

}
