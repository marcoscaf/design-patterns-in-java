package br.com.labmcs.designpatterns.templateMethod;

public class ImpostoCondocionalMinimaTax extends TemplateImpostoCondicional {

	@Override
	protected boolean usaMaximaTaxacao(Orcamento orcamento) {
		// l�gica para minima taxa��o
		if (orcamento.getValorOrcamento() > 100)
			return false;
		else
			return true;
	}

}
