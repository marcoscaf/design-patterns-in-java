package br.com.labmcs.designpatterns.templateMethod;


/**
 * Contrato do Strategy
 */
public interface Imposto {
	
	public double calculaImporto(Orcamento orcamento);

}
