package br.com.labmcs.designpatterns.templateMethod;

public abstract class TemplateImpostoCondicional implements Imposto {

	public double calculaImporto(Orcamento orcamento) {
		
		double valorFinal;
		
		if(usaMaximaTaxacao(orcamento)) {
			
			valorFinal = 100;
			
		} else {
			valorFinal = 50;
		}
		return valorFinal;
	}
	
	protected abstract boolean usaMaximaTaxacao(Orcamento orcamento);

}
